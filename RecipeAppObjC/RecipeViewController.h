//
//  RecipeViewController.h
//  RecipeAppObjC
//
//  Created by Garrick McMickell on 9/28/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeViewController : UIViewController
@property (nonatomic, strong) NSDictionary* details;
@property (weak, nonatomic) IBOutlet UITextView *textview;
@property (weak, nonatomic) IBOutlet UITextView *textview1;

@end
