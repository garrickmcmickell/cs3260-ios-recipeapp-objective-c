//
//  AppDelegate.h
//  RecipeAppObjC
//
//  Created by Garrick McMickell on 9/28/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

