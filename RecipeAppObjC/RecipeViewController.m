//
//  RecipeViewController.m
//  RecipeAppObjC
//
//  Created by Garrick McMickell on 9/28/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.title = self.details[@"title"];
    self.textview.text = self.details[@"ingredients"];
    self.textview1.text = self.details[@"directions"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
