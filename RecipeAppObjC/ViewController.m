//
//  ViewController.m
//  RecipeAppObjC
//
//  Created by Garrick McMickell on 9/28/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dictionaryArray = [NSMutableArray new];
    
    NSDictionary* recipe1 = @{@"title": @"Easy Garlic Chicken", @"ingredients": @"1 1/2 pounds skinless, boneless chicken breast halves \n1/4 cup butter \n6 cloves crushed garlic \n2 cups seasoned dry bread crumbs", @"directions": @"1. Preheat oven to 375 degrees F (190 degrees C). \n 2. In a small saucepan melt butter/margarine with garlic. Dip chicken pieces in butter/garlic sauce, letting extra drip off, then coat completely with bread crumbs. \n 3. Place coated chicken in a lightly greased 9x13 inch baking dish. Combine any leftover butter/garlic sauce with bread crumbs and spoon mixture over chicken pieces. Bake in the preheated oven for 45 minutes to 1 hour."};
    NSDictionary* recipe2 = @{@"title": @"Easy Tuna Casserole", @"ingredients": @"3 cups cooked macaroni \n1 (6 ounce) can tuna, drained \n1 (10.75 ounce) can condensed cream of chicken soup \n1 cup shredded Cheddar cheese \n1 1/2 cups French fried onions", @"directions": @"1. Preheat oven to 350 degrees F (175 degrees C). \n2. In a 9x13-inch baking dish, combine the macaroni, tuna, and soup. Mix well, and then top with cheese. \n3. Bake at 350 degrees F (175 degrees C) for about 25 minutes, or until bubbly. Sprinkle with fried onions, and bake for another 5 minutes. Serve hot."};
    NSDictionary* recipe3 = @{@"title": @"Easy Caramelized Onion Pork Chops", @"ingredients": @"1 tablespoon vegetable oil \n4 (4 ounce) pork loin chops, 1/2 inch thick \n3 teaspoons seasoning salt \n2 teaspoons ground black pepper \n1 onion, cut into strips \n1 cup water", @"directions": @"1. Rub chops with 2 teaspoons seasoning salt and 1 teaspoon pepper, or to taste. \n2. In a skillet, heat oil over medium heat. Brown pork chops on each side. Add the onions and water to the pan. Cover, reduce heat, and simmer for 20 minutes. \n3. Turn chops over, and add remaining salt and pepper. Cover, and cook until water evaporates and onions turn light to medium brown. Remove chops from pan, and serve with onions on top."};
    
    [dictionaryArray addObject:recipe1];
    [dictionaryArray addObject:recipe2];
    [dictionaryArray addObject:recipe3];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"celltouched" sender:indexPath];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dictionaryArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* d = dictionaryArray[indexPath.row];
    cell.textLabel.text = d[@"title"];
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"celltouched"]) {
        NSIndexPath* path = (NSIndexPath*)sender;
        RecipeViewController* rvc = (RecipeViewController*)segue.destinationViewController;
        rvc.details = [dictionaryArray objectAtIndex:path.row];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
